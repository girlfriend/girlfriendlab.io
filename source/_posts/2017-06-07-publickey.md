---
title: 公开密钥加密
---
摘抄自维基百科 [公开密钥加密](https://www.wikiwand.com/zh-hans/%E5%85%AC%E5%BC%80%E5%AF%86%E9%92%A5%E5%8A%A0%E5%AF%86)
该思想最早由瑞夫·墨克（Ralph C. Merkle）在1974年提出

公开密钥加密（英语：public-key cryptography，又譯為公開金鑰加密），也称为非对称加密（asymmetric cryptography

## 整数分解
- Benaloh Blum–Goldwasser Cayley–Purser Damgård–Jurik GMR Goldwasser–Micali Paillier Rabin RSA Okamoto–Uchiyama Schmidt–Samoa
## 离散对数
- Cramer–Shoup DH DSA ECDH ECDSA EdDSA EKE ElGamal signature scheme MQV Schnorr SPEKE SRP STS SM2
## 其他
- AE CEILIDH EPOC HFE IES Lamport McEliece Merkle–Hellman Naccache–Stern Naccache–Stern knapsack cryptosystem NTRUEncrypt NTRUSign Three-pass protocol XTR

# 理论	

## 离散对数 椭圆曲线密码学 Non-commutative cryptography RSA problem
## 标准化	
- CRYPTREC IEEE P1363 NESSIE NSA Suite B
## 论题	
- 数位签章 OAEP 密钥指纹 PKI Web of trust Key size Post-quantum cryptography