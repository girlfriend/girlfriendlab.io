---
title: 对称密钥加密
---
摘抄自维基百科 [对称密钥加密](https://www.wikiwand.com/zh-hans/%E5%B0%8D%E7%A8%B1%E5%AF%86%E9%91%B0%E5%8A%A0%E5%AF%86)
该思想最早由瑞夫·墨克（Ralph C. Merkle）在1974年提出

## 常见的对称加密算法
- DES、3DES、AES、Blowfish、IDEA、RC5、RC6
