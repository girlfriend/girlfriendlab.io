---
title: 从 R 中学习数据流加密与混淆
---

### 加密

- table
- rc4
- rc4-md5
- rc4-md5-256
- aes-128-cfb
- aes-192-cfb
- aes-256-cfb
- aes-128-ctr
- aes-192-ctr
- aes-256-ctr
- bf-cfb
- camellia-128-cfb
- camellia-192-cfb
- camellia-256-cfb
- salsa20
- chacha20
- chaxha20-ietf

### 协议

- verify-simple
- verify-sha1
- auth_sha1
- auth_sha1_v2
- auth_sha1_v4
- auth_aes128_sha1
- auth_aes128_md5
- auth_chain_a

### 混淆

- http_simple
- http_post
- tls_simple
- tls1.2_ticket_auth
